'use strict'

module.exports = function(app) {
    //GET routes to obtain temperature and humidity
    app.get('/getTemperatureData', require('./models/temperature').getData);
    app.get('/getHumidityData', require('./models/humidity').getData);

    //POST routes to send messages to registered MQTT clients
    app.post("/sendToClient", require('./mqtt/handler').sendMessage);
}
