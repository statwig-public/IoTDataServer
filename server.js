const express = require('express'),
    bodyParser = require('body-parser'),
    ArrayList = require('arraylist'),
    fs = require('fs'),
    mergeJSON = require("merge-json"),
    app = express();

const PORT = process.env.PORT || 5000;

app.use(bodyParser.json());

const routes = require('./routes');
routes(app);

app.listen(PORT, () => console.log(`Listening on ${PORT}`));