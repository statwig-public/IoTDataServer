Run 'npm install' to install all the packages listed in package.json

Run 'npm run dev' after installation to start the server.

routes.js contains all the available REST API paths

server.js uses express to bring up the server on default port 5000

This server uses models to simulate sensor data for temperature and humidity. This is published to the multichain through
REST APIs.

![alt text](https://gitlab.com/statwig-public/IoTDataServer/blob/master/architecture.jpeg)
