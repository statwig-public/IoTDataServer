const mqtt = require('mqtt'),
    dotenv = require('dotenv').config();

class MqttHandler {
  constructor() {
    this.mqttClient = process.env.CLIENT;
    this.host = process.env.HOST;
    this.username = process.env.USERNAME;
    this.password = process.env.PASSWORD;
  }
  
  connect() {
    this.mqttClient = mqtt.connect(this.host,  { username: this.username, password: this.password });

    this.mqttClient.on('error', (err) => {
      console.log(err);
      this.mqttClient.end();
    });

    this.mqttClient.on('connect', () => {
      console.log("MQTT client connected");
    });

    this.mqttClient.subscribe('temperature_data', {qos: 1});

    this.mqttClient.on('message', function (topic, message) {
      console.log(message.toString());
    });

}
  sendMessage(message) {
    this.mqttClient.publish('temperature_data', message, {qos: 2});
    return message;
  }
}

module.exports = MqttHandler;
