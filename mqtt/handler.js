var mqttHandler = require('./connection');

exports.sendMessage = function(req, res) {
    var mqttClient = new mqttHandler();
    mqttClient.connect();
    const message = mqttClient.sendMessage(req.body.message);
    res.sendStatus(200);
}
